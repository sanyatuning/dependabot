# frozen_string_literal: true

class Version
  VERSION = "0.31.1"
end
