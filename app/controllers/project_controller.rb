# frozen_string_literal: true

class ProjectController < ApplicationController
  def add
    access_token = params[:access_token].presence
    project = Dependabot::Projects::Creator.call(params.require(:project_name), access_token: access_token)
    if project.configuration
      Cron::JobSync.call(project)
      redirect_to root_path, notice: "Project added / updated"
    else
      redirect_to root_path, notice: "No configuration in the project"
    end
  rescue Gitlab::Error::Error => e
    redirect_to root_path, alert: e.message
  end

  def update
    project = Project.find_by(id: params.require(:id))

    Dependabot::Projects::Creator.call(project.name, access_token: project.gitlab_access_token)
    Cron::JobSync.call(project)

    redirect_to root_path
  end
end
