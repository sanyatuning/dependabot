# Getting started

This section shows the simplest scenario to start using dependabot-gitlab

## Step 1: Add configuration file

Add configuration file `.gitlab/dependabot.yml` to the project. Minimal configuration requires following options to be present:

```yaml
version: 2
updates:
  - package-ecosystem: package-manager
    directory: /
    schedule:
      interval: daily
```

## Step 2: Set up access tokens

- Create [gitlab personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
- Create [github personal access token](https://docs.github.com/en/github/authenticating-to-github/creating-a-personal-access-token)

## Step 3: Start the app

Clone the project:

```sh
git clone https://gitlab.com/dependabot-gitlab/dependabot.git && cd dependabot
```

Save gitlab url to environment variable `SETTINGS__GITLAB_URL`:

```sh
export SETTINGS__GITLAB_URL=https://gitlab.com
```

Save gitlab access token to environment variable `SETTINGS__GITLAB_ACCESS_TOKEN`:

```sh
export SETTINGS__GITLAB_ACCESS_TOKEN=gitlab_access_token
```

Save github access token to environment variable `SETTINGS__GITHUB_ACCESS_TOKEN`:

```sh
export SETTINGS__GITHUB_ACCESS_TOKEN=github_access_token
```

Start app using [docker compose](https://docs.docker.com/compose/):

```sh
docker compose up -d
```

## Step 4: Add project

```sh
docker compose run --rm web bundle exec rake dependabot:register_project[project_name]
```
